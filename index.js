const app = require("./app")

const port = 5001
app.listen(port, () => {
    console.log(`app listening on port ${port}`)
})
